---
- name: Install GitLab Runner repository
  ansible.builtin.yum_repository:
    name: 'gitlab-runner'
    description: 'gitlab-runner'
    baseurl: '{{ gitlab_runner_baseurl }}'
    repo_gpgcheck: true
    gpgcheck: true
    gpgkey: '{{ gitlab_runner_repo_gpgkey }}'
    metadata_expire: '300'
    state: 'present'
  when: ansible_distribution in ('CentOS', 'Rocky')
  become: true
- name: Install GitLab Runner
  ansible.builtin.package:
    name: 'gitlab-runner'
    state: 'present'
  notify: 'Restart GitLab Runner'
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
  become: true
- name: Add gitlab-runner user to docker group
  ansible.builtin.user:
    append: true
    groups: 'docker'
    name: 'gitlab-runner'
  notify: 'Restart GitLab Runner'
  when: ansible_distribution in ('CentOS', 'Rocky')
  become: true
- name: Enable GitLab Runner
  ansible.builtin.service:
    name: 'gitlab-runner'
    enabled: true
    state: 'started'
  become: true
- name: Get registered GitLab Runners
  ansible.builtin.shell: 'gitlab-runner list'  # noqa 305
  register: gitlab_runners_res
  become: true
  changed_when: false
- name: Register GitLab Runners
  ansible.builtin.command: >
    gitlab-runner register -n
    {{ item.keys() | map('replace', '_', '-') | map('regex_replace', '^', '--')
    | zip(item.values() | map('regex_replace', '^(.*)$', '"\1"'))
    | map('join','=') | reject('search', '^--state=.*') | join(' ') }}
  when: >
    gitlab_runner is defined
    and not gitlab_runners_res.stderr is search('\n' ~ item.name | regex_escape() ~ '.+')
    and item.state | default('present') | lower == 'present'
  with_items: '{{ gitlab_runner.runners | default([]) }}'
  no_log: true
  become: true
- name: Unregister runners
  ansible.builtin.command: >
    gitlab-runner unregister
    {{ item.keys() | map('replace', '_', '-') | map('regex_replace', '^', '--')
    | zip(item.values() | map('regex_replace', '^(.*)$', '"\1"'))
    | map('join','=') | reject('search', '^--state=.*') | join(' ') }}
  when: >
    gitlab_runner is defined
    and gitlab_runners_res.stderr is search('\n' ~ item.name | regex_escape() ~ '.+')
    and item.state | default('present') | lower == 'absent'
  with_items: '{{ gitlab_runner.runners | default([]) }}'
  no_log: true
  become: true
